package storage

import (
	"errors"
	"reflect"
	"sync"
)

/*
	Интерфес хранилища, реализует методы CRUD
	Создание элемента:
		Create(key interface{}, value interface{}) error
	Получение элемента:
		Get(key interface{}) (interface{}, error)
	Обновление элемента:
		Update(key interface{}, value interface{}) error
	Удаление элемента:
		Delete(key interface{}) error
*/
type Storage interface {
	Create(key interface{}, value interface{}) error
	Get(key interface{}) (interface{}, error)
	Update(key interface{}, value interface{}) error
	Delete(key interface{}) error
}

/*
	Структура хранилища, содержит map для данных и мютекс
*/
type MemoriStorage struct {
	sync.RWMutex
	storage map[interface{}]interface{}
}

/*
	Инициализаия хранилища
	Возвращает интерфейс Storange
	Использование:
		s := storage.InitMemoryStorage()

*/
func InitMemoryStorage() Storage {
	return &MemoriStorage{storage: make(map[interface{}]interface{})}
}

/*
	Добавление элемента в хранилище.
	Получает ключ и значение.
	Возвращает ошибку в случае если элемент с полученым ключом уже есть в хранилище и nil в случае успешного добавления.
	Использование:
		err := s.Create("k", "v")
*/
func (s *MemoriStorage) Create(key interface{}, value interface{}) error {
	if reflect.ValueOf(key).Kind() == reflect.Map {
		return errors.New("key is map")
	}
	s.Lock()
	defer s.Unlock()

	if _, ok := s.storage[key]; ok {
		return errors.New("key is exist")
	}
	s.storage[key] = value
	return nil
}

/*
	Получение элемента из хранилища.
	Получает ключ.
	Возвращает значение и nil в случае обнаружения элемента и (nil, error) в случае отсутсвия элемента в хранилище.
	Использование:
		v, err := s.Get("k")
*/
func (s *MemoriStorage) Get(key interface{}) (interface{}, error) {
	if reflect.ValueOf(key).Kind() == reflect.Map {
		return nil, errors.New("key is map")
	}
	s.RLock()
	defer s.RUnlock()
	if v, ok := s.storage[key]; ok {
		return v, nil
	}
	return nil, errors.New("empty key")
}

/*
	Обновление значения элемента по ключу.
	Получает ключ для поиска и новое значение.
	Возвращает nil в случае успешного обновления и error в случае отсутствия элемента в хранилище.
	Использование:
		err := s.Update("k", "v2")
*/
func (s *MemoriStorage) Update(key interface{}, value interface{}) error {
	if reflect.ValueOf(key).Kind() == reflect.Map {
		return errors.New("key is map")
	}
	s.Lock()
	defer s.Unlock()
	if _, ok := s.storage[key]; !ok {
		return errors.New("empty key")
	}
	s.storage[key] = value
	return nil
}

/*
	Удаление элемента из хранилища.
	Получает ключ элемента.
	Возвращает nil в случае успешного удаления и error в случае отсутствия элемента в хранилище.
	Использование:
		err := s.Delete("k")
*/
func (s *MemoriStorage) Delete(key interface{}) error {
	if reflect.ValueOf(key).Kind() == reflect.Map {
		return errors.New("key is map")
	}
	s.Lock()
	defer s.Unlock()
	if _, ok := s.storage[key]; !ok {
		return errors.New("empty key")
	}
	delete(s.storage, key)
	return nil
}
