# Реализация модуля Storage

Модуль реализует in-memory key-value хранилище.

## Установка
```bash
go get gitlab.com/Eluvium/storage
```
## Пример использования


```go
package main

import (
	"fmt"

	"gitlab.com/Eluvium/storage"
)

func main() {
    //Инициализация хранилища
	s := storage.InitMemoryStorage()
    //Добавление элемента
	err := s.Create("test", "value")
	if err != nil {
		fmt.Println("Create error")
	}
    //Получение значения по ключу
	v, err := s.Get("test")
	if err != nil {
		fmt.Println("Get error")
		return
	} else {
		fmt.Println("Get value:", v)
	}
	//Обновление значения элемента
	err = s.Update("test", "v2")
	if err != nil {
		fmt.Println("Update error")
		return
	}
	//Удаление элемента
	err = s.Delete("test")
	if err != nil {
		fmt.Println("Delete error")
		return
	}
}

```