package storage

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

var s Storage

func TestInitMemoryStorage(t *testing.T) {
	s = InitMemoryStorage()
	if s == nil {
		t.Errorf("Not init memorystorage")
	}
}

func TestCreate(t *testing.T) {
	tests := []struct {
		inputKey      interface{}
		inputValue    interface{}
		expectedError error
	}{
		{inputKey: "Key 1", inputValue: "Value 1", expectedError: nil},
		{inputKey: "Key 2", inputValue: 2, expectedError: nil},
		{inputKey: 3, inputValue: "3", expectedError: nil},
		{inputKey: 3, inputValue: "3", expectedError: errors.New("key is exist")},
		{inputKey: 3.4, inputValue: 3.4, expectedError: nil},
		{inputKey: 4, inputValue: map[string]string{"a": "b"}, expectedError: nil},
		{inputKey: nil, inputValue: map[string]string{"a": "b"}, expectedError: nil},
		{inputKey: map[string]string{"a": "b"}, inputValue: map[string]string{"a": "b"}, expectedError: errors.New("key is map")},
	}
	for _, tt := range tests {
		actualError := s.Create(tt.inputKey, tt.inputValue)

		assert.Equal(t, tt.expectedError, actualError, "Create test error")
	}
}

func TestGet(t *testing.T) {
	tests := []struct {
		inputKey      interface{}
		outValue      interface{}
		expectedError error
	}{
		{inputKey: "Key 1", outValue: "Value 1", expectedError: nil},
		{inputKey: "Key 2", outValue: 2, expectedError: nil},
		{inputKey: 3, outValue: "3", expectedError: nil},
		{inputKey: 3.4, outValue: 3.4, expectedError: nil},
		{inputKey: 4, outValue: map[string]string{"a": "b"}, expectedError: nil},
		{inputKey: nil, outValue: map[string]string{"a": "b"}, expectedError: nil},
		{inputKey: map[string]string{"a": "b"}, outValue: nil, expectedError: errors.New("key is map")},
		{inputKey: 5, outValue: nil, expectedError: errors.New("empty key")},
	}
	for _, tt := range tests {
		actualValue, actualError := s.Get(tt.inputKey)

		assert.Equal(t, tt.expectedError, actualError, "Get test error")
		assert.Equal(t, tt.outValue, actualValue, "Get test error")

	}
}

func TestUpdate(t *testing.T) {
	tests := []struct {
		inputKey      interface{}
		inputValue    interface{}
		expectedError error
	}{
		{inputKey: "Key 1", inputValue: "Value 2", expectedError: nil},
		{inputKey: "Key 2", inputValue: 4, expectedError: nil},
		{inputKey: 3, inputValue: "5", expectedError: nil},
		{inputKey: 3.4, inputValue: 3.6, expectedError: nil},
		{inputKey: 4, inputValue: map[string]string{"c": "c"}, expectedError: nil},
		{inputKey: nil, inputValue: map[string]string{"c": "b"}, expectedError: nil},
		{inputKey: map[string]string{"a": "b"}, inputValue: map[string]string{"a": "b"}, expectedError: errors.New("key is map")},
		{inputKey: 5, inputValue: 5, expectedError: errors.New("empty key")},
	}
	for _, tt := range tests {
		actualError := s.Update(tt.inputKey, tt.inputValue)

		assert.Equal(t, tt.expectedError, actualError, "Create test error")
	}
}

func TestDelete(t *testing.T) {
	tests := []struct {
		inputKey      interface{}
		expectedError error
	}{
		{inputKey: "Key 1", expectedError: nil},
		{inputKey: "Key 2", expectedError: nil},
		{inputKey: 3, expectedError: nil},
		{inputKey: 3.4, expectedError: nil},
		{inputKey: 4, expectedError: nil},
		{inputKey: nil, expectedError: nil},
		{inputKey: map[string]string{"a": "b"}, expectedError: errors.New("key is map")},
		{inputKey: 5, expectedError: errors.New("empty key")},
	}
	for _, tt := range tests {
		actualError := s.Delete(tt.inputKey)

		assert.Equal(t, tt.expectedError, actualError, "Create test error")
	}
}
